#ifndef IMAGE_TRANSFORMER_OPEN_FILE_H

#include "bmp.h"
#include "image.h"
#include <bits/types/FILE.h>
#include <stdbool.h>

enum file_work_status {
    FILE_WORK_OK = 0,
    CONVERT_ERROR,
    NULL_FILE_ERROR
};

enum file_work_status open_file(char *mode, char *string, struct image* image, enum convert_status (convert_to_type) (FILE*, struct image*));

#define IMAGE_TRANSFORMER_OPEN_FILE_H

#endif 
