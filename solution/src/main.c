#include "bmp.h"
#include "image.h"
#include "open_file.h"
#include "rotate_transform.h"
#include "util.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv ) {
    if (argc < 3) return 2;
    
    struct image old_image;
    
    enum file_work_status const read_ans = open_file("rb", argv[1], &old_image, from_bmp_to_image);
    if (read_ans != FILE_WORK_OK) {
    	return read_ans;
    }
    
    struct image new_image;
    struct pixel data[old_image.width*old_image.height];
	new_image.data = data;
	
    copy_data(&old_image, &new_image);
    
    rotate(&old_image, &new_image, -1);
    
    enum file_work_status const write_ans = open_file("wb", argv[2], &new_image, from_image_to_bmp);
    if (write_ans != FILE_WORK_OK) {
        return write_ans;
    }
	free(old_image.data);
    return 0;
}

