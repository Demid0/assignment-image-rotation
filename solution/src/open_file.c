#include "open_file.h"
#include <stdio.h>

enum file_work_status open_file(char * mode, char *string, struct image* image, enum convert_status (convert_to_type) (FILE*, struct image*)) {
    FILE* fo = fopen(string, mode);
    if (fo == NULL) return NULL_FILE_ERROR;
    else {
        enum convert_status const ans = convert_to_type(fo, image);
        if (ans == CONVERT_OK) return FILE_WORK_OK;
        else {
            printf("%u", ans);
            return CONVERT_ERROR;
        }
    }
}
